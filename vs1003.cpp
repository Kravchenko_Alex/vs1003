#include "vs1003.h"


/* --------------------------------------------- GLOBAL VARIABLES -------------------------------------------------- */
uint8_t VS1003_MUTLIPLIER = 2;
std::atomic<bool> RecordON;

/* -----------------------------------------------------------  USER FUNCTIONS ------------------------------------------------------ */

int initBCM()
{
    if (!bcm2835_init())
    {
        printf("bcm2835_init failed. Are you running as root??\n");
        return 1;
    }
    if (!bcm2835_spi_begin())
    {
        printf("bcm2835_spi_begin failed. Are you running as root??\n");
        return 1;
    }
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // The default
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_1024);  // The default
    bcm2835_spi_chipSelect(BCM2835_SPI_CS_NONE);                  // Select self control of CS pins

    bcm2835_gpio_fsel(RPI_GPIO_P1_24, BCM2835_GPIO_FSEL_OUTP);    // CE0 - xCS
    bcm2835_gpio_fsel(RPI_GPIO_P1_26, BCM2835_GPIO_FSEL_OUTP);    // CE1 - xDCS
    bcm2835_gpio_fsel(RPI_GPIO_P1_22, BCM2835_GPIO_FSEL_INPT);    // DREQ
    bcm2835_gpio_fsel(RPI_GPIO_P1_18, BCM2835_GPIO_FSEL_OUTP);    // XRESET

    return 0;
}

void close()
{
    bcm2835_spi_end();
    bcm2835_close();
}

void _activateADPCM(bool line_in)
{
    setVolume(25);
    _writeRegister(SCI_BASS, 0);             /* Bass/treble disabled */
    _writeRegister(SCI_CLOCKF, 0x4000);      /* Set Clock Multiplier 2.0x 12.288MHz */
    delayMicroseconds(896);   // Wait 896 us
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_128);   // For clock multiplier x2.0 (SPI max 3.5MHz)

    _writeRegister(SCI_AICTRL0, 6);  // set clock divider (with Clock Mult x2.0 and Div -> 12=8kHz 8=12kHz 6=16kHz
    _writeRegister(SCI_AICTRL1, 0);  // set automatic gain control (AGC)
    if (line_in)
        _writeRegister(SCI_MODE, (_BV(SM_RESET) | _BV(SM_SDINEW) | _BV(SM_ADPCM) | _BV(SM_LINE_IN)));  // set line input
    else
        _writeRegister(SCI_MODE, (_BV(SM_RESET) | _BV(SM_SDINEW) | _BV(SM_ADPCM)));	// set internal microphone input
    delayMicroseconds(1350);                  // Wait 1.35ms
    _waitDREQ();
}

void recordingThread(bool line_in)
{
    std::thread rec (startRecordADPCM, line_in);
//    printf("Enter any to stop record\n");
//    getchar();
    //getchar();
    RecordON.store(false);
    rec.join();
}

void startRecordADPCM(bool line_in)
{
    HeaderWAV header;
    unsigned char db[512]; /* data buffer for saving to disk */
    //char fileName[80];
    uint16_t tmp16 = 0, idx = 0;
    uint32_t numBlocks = 0;

//    time_t t = time(0);   // get time now
//    struct tm * now = localtime(&t);
//    strftime(fileName, 80, "%Y-%m-%d %H:%M:%S.wav", now);
//    FILE* writeFile = fopen(fileName, "wb");
    FILE* writeFile = fopen("test_threadDEL.wav", "wb");
    if (writeFile == NULL)
        Exception("Cannot create file \n");

    fseek(writeFile, 60, SEEK_SET);  // reserve block for header

    _activateADPCM(line_in);
    /* Record loop */
    RecordON.store(true);
    //printf("Start record");
    clock_t start = clock();
    while (((double)(clock() - start) / CLOCKS_PER_SEC) < 20.0) {
//    while(RecordON.load())
//    {
        do
        {
            tmp16 = _readRegister(SCI_HDAT1);
        } while (tmp16 < 256 || tmp16 >= 896); /* wait until 512 bytes available */
        while (idx < 512)
        {
            tmp16 = _readRegister(SCI_HDAT0);
            db[idx++] = tmp16 >> 8;
            db[idx++] = tmp16 & 0xFF;
        }
        idx = 0;
        numBlocks += 2; // 512 bytes = 2*(128 words)
        fwrite(db, sizeof(uint8_t), 512, writeFile);   /* Write output block to disk */
    }

     /* Fix WAV header information */
    header.chunkSize = (numBlocks * 256) + 52;
    header.nSamplesPerSec = 0x3E80;   // 16 kHz
    header.nAvgBytesPerSec = 0x5F0CAC;  // for 16 kHz
    header.numberOfSamples = numBlocks * 505;
    header.subChunk_3_Size = numBlocks * 256;

    fseek(writeFile, 0, SEEK_SET);
    fwrite(header.headerIter, sizeof(uint8_t), 60, writeFile); /* Save header */

    fclose(writeFile);

    swReset(); /* Normal reset, restore default settings */
}

void readVersion()
{
    uint16_t version = _readRegister(SCI_STATUS);
    version &= (_BV(4) | _BV(5) | _BV(6));
    version >>= 4;
    printf("Version: %u \n", version);
}

void swReset()
{
    _writeRegister(SCI_MODE, (_BV(SM_RESET) | _BV(SM_SDINEW)));
    delayMicroseconds(1350);                  // Wait 1.35ms
    _waitDREQ();
}


void hwReset()
{
    bcm2835_gpio_write(RPI_GPIO_P1_24, HIGH); // Stop trasmitting SCI
    bcm2835_gpio_write(RPI_GPIO_P1_26, HIGH); // Stop trasmitting SDI
    bcm2835_gpio_write(RPI_GPIO_P1_18, LOW);
    delayMicroseconds(2);                     // ?? How long it should be
    bcm2835_gpio_write(RPI_GPIO_P1_18, HIGH);
    delayMicroseconds(1350);                  // Wait 1.35ms
    _waitDREQ();
}


void setGPIO(uint8_t pin, uint8_t direction)
{
    _writeRegister(SCI_WRAMADDR, GPIO_DDR);
    uint16_t currDDR = _readRegister(SCI_WRAM);
    uint16_t pinMask = 0;
    switch (pin)
    {
    case 0: { pinMask |= _BV(0); break; }
    case 1: { pinMask |= _BV(1); break; }
    case 2: { pinMask |= _BV(2); break; }
    case 3: { pinMask |= _BV(3); break; }
    default: { printf("Pin number should be between 0 and 3 "); return; }
    }
    if (direction)             //Output
        currDDR |= pinMask;
    else                       //Input
        currDDR &= ~pinMask;

    _writeRegister(SCI_WRAMADDR, GPIO_DDR);  // ??? Set pointer back because of autoincremention (look chapter 8.6.7)
    _writeRegister(SCI_WRAM, currDDR);       // Write value back to register
}


void writeGPIO(uint8_t pin, uint8_t state)
{
    _writeRegister(SCI_WRAMADDR, GPIO_ODATA);
    uint16_t pinsState = _readRegister(SCI_WRAM);
    uint16_t pinMask = 0;
    switch (pin)
    {
    case 0: { pinMask |= _BV(0); break; }
    case 1: { pinMask |= _BV(1); break; }
    case 2: { pinMask |= _BV(2); break; }
    case 3: { pinMask |= _BV(3); break; }
    default: { printf("Pin number should be between 0 and 3 "); return; }
    }
    if (state)                 //HIGH
        pinsState |= pinMask;
    else                       //LOW
        pinsState &= ~pinMask;

    _writeRegister(SCI_WRAMADDR, GPIO_ODATA);  // ??? Set pointer back because of autoincremention (look chapter 8.6.7)
    _writeRegister(SCI_WRAM, pinsState);       // Write value back to register
}


bool readGPIO(uint8_t pin)
{
    _writeRegister(SCI_WRAMADDR, GPIO_IDATA);
    uint16_t pinsState = _readRegister(SCI_WRAM);
    uint16_t pinMask = 0;
    switch (pin)
    {
    case 0: { pinMask |= _BV(0); break; }
    case 1: { pinMask |= _BV(1); break; }
    case 2: { pinMask |= _BV(2); break; }
    case 3: { pinMask |= _BV(3); break; }
    default: { printf("Pin number should be between 0 and 3 "); return 1; }
    }
    pinsState &= pinMask;
    if (pinsState) // if pinsState > 0 that means chosen pin has state HIGH
        return HIGH;
    else
        return LOW;

}

void setVolume(uint8_t vol)
{
    if (vol == 255)
    {
        printf("Volume must be between 0 and 254 ");
        return;
    }
    uint16_t value = vol;
    value <<= 8;    // pow to 256 (for left channel)
    value |= vol;

    _writeRegister(SCI_VOL, value); // VOL
}

/* --------------------------------------------  LOW LEVEL COMMANDS ------------------------------------------------ */

uint64_t cyclesToMicroseconds(uint64_t numCycles)
{
    return (numCycles * 1000000) / (12288000 * VS1003_MUTLIPLIER);
}

void _waitDREQ()
{
    //printf("DREQ val: %u \n", bcm2835_gpio_lev(RPI_GPIO_P1_22));
    while (!bcm2835_gpio_lev(RPI_GPIO_P1_22))
        ;
}

void _setDataMode(uint8_t state)
{
    bcm2835_gpio_write(RPI_GPIO_P1_24, HIGH);
    if (state)
        bcm2835_gpio_write(RPI_GPIO_P1_26, LOW);
    else
        bcm2835_gpio_write(RPI_GPIO_P1_26, HIGH);
}


void _setControlMode(uint8_t state)
{
    bcm2835_gpio_write(RPI_GPIO_P1_26, HIGH);
    if (state)
        bcm2835_gpio_write(RPI_GPIO_P1_24, LOW);
    else
        bcm2835_gpio_write(RPI_GPIO_P1_24, HIGH);
}


void _writeRegister(uint8_t reg, uint16_t value)
{
    _waitDREQ();
    _setControlMode(ON);
    delayMicroseconds(1);                    // tXCSS

    bcm2835_spi_transfer(VS_WRITE_COMMAND);  // Write operation
    bcm2835_spi_transfer(reg);               // Which register
    bcm2835_spi_transfer(value >> 8);        // Send hi byte
    bcm2835_spi_transfer(value & 0xFF);      // Send lo byte
    delayMicroseconds(1);                    // tXCSH
    _setControlMode(OFF);
}


uint16_t _readRegister(uint8_t reg)
{
    uint16_t result;
    _waitDREQ();
    _setControlMode(ON);
    delayMicroseconds(1);                     // tXCSS
    bcm2835_spi_transfer(VS_READ_COMMAND);    // Read operation
    bcm2835_spi_transfer(reg);                // Which register
    result = bcm2835_spi_transfer(0x0) << 8;  // read high byte
    result |= bcm2835_spi_transfer(0x0);      // read low byte
    //delayMicroseconds(1);                     // tXCSH
    _setControlMode(OFF);
    return result;
}


void sendData(const uint8_t* data, size_t len)
{
    _setDataMode(ON);
    while (len)
    {
        _waitDREQ();
        delayMicroseconds(1);  // ??????

        size_t blockSize = MIN(len, SDI_BLOCK_SIZE);
        len -= blockSize;
        while (blockSize--)
            bcm2835_spi_transfer(*data++);

        if ((len % 2048) == 0)  // for additional synchronization
        {
            _setDataMode(ON);
            delayMicroseconds(1);
            _setDataMode(OFF);
        }
    }
    _setDataMode(OFF);
}
