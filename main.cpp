#include <QCoreApplication>
#include "vs1003.h"

int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);

    initBCM();
    //hwReset();
    delay(10);
//    _writeRegister(SCI_CLOCKF, 0x4000);   // Set Clock Multiplier x2.0
//    delayMicroseconds(896); // 11000 CLKI == 896 microseconds
//    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_128);   // For clock multiplier x2.0 (SPI max 3.5MHz)

    /* ------- Recording Thread Testing -------*/
    recordingThread(OFF);
    printf("Ready\n");

    /* ------- Recording Testing -------*/
//    startRecordADPCM(OFF);
//    printf("Ready \n");
    /* ------- GPIO Testing -------*/
//    setGPIO(1,OUTPUT);
//    while(1)
//    {
//        writeGPIO(1, HIGH);
//        delay(200);
//        writeGPIO(1, LOW);
//        delay(200);
//    }
//    writeGPIO(1, LOW);

    close();

    //return a.exec();
    return 0;
}
