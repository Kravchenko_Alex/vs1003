#ifndef VS1003_H
#define VS1003_H

#include "bcm2835.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <atomic>
#include <thread>
#include <mutex>

union HeaderWAV
{
    uint16_t headerIter[30];   // 60 bytes == 30 words
    struct
    {
        uint32_t chunkID;  /* 'RIFF' */
        uint32_t chunkSize;  /* = fileSize - 8 bytes*/
        uint32_t format; /* 'WAVE' */
        uint32_t subChunk_1_ID;  /* 'fmt ' */
        uint32_t subChunk_1_Size;  /* 20 bytes*/
        uint16_t formatTag; /* format type IMA ADPCM */
        uint16_t nChannels; /* number of channels (i.e. mono, stereo...) | MONO SOUND ??? */
        uint32_t nSamplesPerSec; /* sample rate */
        uint32_t nAvgBytesPerSec; /* byte ratefor buffer estimation */
        uint16_t nBlockAlign; /* block size of data */
        uint16_t nBitsPerSample; /* Number of bits per sample of mono data | 4-bit ADPCM */
        uint16_t cbSize; /* The count in bytes of the extra size */
        uint16_t nSamplesPerBlock;   // 505
        uint32_t subChunk_2_ID;  /* 'fact' */
        uint32_t subChunk_2_Size;  /* 4 bytes*/
        uint32_t numberOfSamples; /* number of samples */
        uint32_t subChunk_3_ID;  /* 'data' */
        uint32_t subChunk_3_Size;  /* fileSize - 60 bytes */
    };

    HeaderWAV()
    {
        chunkID = 0x46464952;
        format = 0x45564157;
        subChunk_1_ID = 0x20746D66;
        subChunk_1_Size = 0x14;
        formatTag = 0x0011;
        nChannels = 0x0001;
        nBlockAlign = 0x100;
        nBitsPerSample = 0x0004;
        cbSize = 2;
        nSamplesPerBlock = 0x01F9;
        subChunk_2_ID = 0x74636166;
        subChunk_2_Size = 0x4;
        subChunk_3_ID = 0x61746164;
    }
};

/* -------------------------------------------- ARGUMENT DEFINITIONS ----------------------------------------------- */
#define ON  0x1
#define OFF 0x0
#define OUTPUT 0x1
#define INPUT  0x0

/* ------------------------------------------------- MACROS -------------------------------------------------------- */
#define MAX(a, b)   ((a) > (b) ? (a) : (b))
#define MIN(a, b)   ((a) < (b) ? (a) : (b))
#define _BV(bit) (1 << (bit))
#define Exception(msg)  { perror(msg); exit(EXIT_FAILURE); }

/* ----------------------------------------------- SCI Commands ---------------------------------------------------- */

#define VS_WRITE_COMMAND 0x02        // VS1003 SCI Write Command byte is 0x02
#define VS_READ_COMMAND  0x03        // VS1003 SCI Read Command byte is 0x03


/* ------------------------------------------------- SCI Registers ------------------------------------------------- */
const uint8_t SCI_MODE = 0x0;
const uint8_t SCI_STATUS = 0x1;
const uint8_t SCI_BASS = 0x2;
const uint8_t SCI_CLOCKF = 0x3;
const uint8_t SCI_DECODE_TIME = 0x4;
const uint8_t SCI_AUDATA = 0x5;
const uint8_t SCI_WRAM = 0x6;
const uint8_t SCI_WRAMADDR = 0x7;
const uint8_t SCI_HDAT0 = 0x8;
const uint8_t SCI_HDAT1 = 0x9;
const uint8_t SCI_AIADDR = 0xA;
const uint8_t SCI_VOL = 0xB;
const uint8_t SCI_AICTRL0 = 0xC;
const uint8_t SCI_AICTRL1 = 0xD;
const uint8_t SCI_AICTRL2 = 0xE;
const uint8_t SCI_AICTRL3 = 0xF;

/* ---------------------------------------------- SCI_MODE Bit Positions ------------------------------------------ */
const uint8_t SM_DIFF = 0;
const uint8_t SM_LAYER12 = 1;
const uint8_t SM_RESET = 2;
const uint8_t SM_OUTOFWAV = 3;
const uint8_t SM_EARSPEAKER_LO = 4;
const uint8_t SM_TESTS = 5;
const uint8_t SM_STREAM = 6;
const uint8_t SM_EARSPEAKER_HI = 7;
const uint8_t SM_DACT = 8;
const uint8_t SM_SDIORD = 9;
const uint8_t SM_SDISHARE = 10;
const uint8_t SM_SDINEW = 11;
const uint8_t SM_ADPCM = 12;
const uint8_t SM_ADCPM_HP = 13;
const uint8_t SM_LINE_IN = 14;


/* ------------------------------------------------- GPIO Registers  ----------------------------------------------- */
const uint16_t GPIO_DDR   = 0xC017;
const uint16_t GPIO_IDATA = 0xC018;
const uint16_t GPIO_ODATA = 0xC019;


// defines size of data block that VS1003 can recieve for decoding (SDI data) if DREQ is HIGH
const uint8_t SDI_BLOCK_SIZE = 32;

/* ------------------------------------------- FUNCTION PROTOTYPES ------------------------------------------------- */
/*
----------------------------------------------------------------------------------------------------------------------
*/
/* -----------------------------------------------  USER FUNCTIONS ------------------------------------------------- */

int initBCM();
void close();
void _activateADPCM(bool line_in);
void recordingThread(bool line_in);
void startRecordADPCM(bool line_in);
//void stopRecordADPCM();
void readVersion();
void swReset();
void hwReset();
void setGPIO(uint8_t pin, uint8_t direction);
void writeGPIO(uint8_t pin, uint8_t state);
bool readGPIO(uint8_t pin);
void setVolume(uint8_t vol);



/* -----------------------------------------  LOW LEVEL COMMANDS --------------------------------------------------- */

uint64_t cyclesToMicroseconds(uint64_t numCycles);

// basic functions necessary for SCI/SDI commands
void _waitDREQ();
void _setDataMode(uint8_t state);
void _setControlMode(uint8_t state);

// functions that performs SCI read/write commands
void _writeRegister(uint8_t reg, uint16_t value);
uint16_t _readRegister(uint8_t reg);

// function that performs SDI command (send data for decoding)
void _sendData(const uint8_t* data, size_t len);


#endif // VS1003_H
